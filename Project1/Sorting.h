#pragma once

#include "SFML/Graphics.hpp"

#include "Helpers.h"

#include <vector>
#include <iostream>

class Sorting
{
public:

	Sorting();

	~Sorting();

public:

	void Render(sf::RenderWindow &l_renderWindow);

	void AddStick();

	void BubbleSort();

	void InsertionSort();

private:

	sf::RectangleShape m_line;

	std::vector<sf::RectangleShape> m_lines;

	float m_increment;

	bool m_Sorted;
	
	const float height;

	int m_stickAmount;

	int m_maxSize;

	unsigned int m_j;

	unsigned int  m_i;

	unsigned int m_rectWidth;

	float m_key;

};

