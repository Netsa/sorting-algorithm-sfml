#pragma once

#include <SFML/Graphics.hpp>

#include "Window.h"
#include "Sorting.h"

class Game
{
public:

	Game();

	~Game();

public:

	void HandleInput();

	void Update();

	void Render();

	Window* GetWindow();

	void ResetClock();

private:

	Window m_window;

	sf::CircleShape m_circle;

	Sorting m_sorting;

	float m_elapsed;

	sf::Clock m_clock;

};

