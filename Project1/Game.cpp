#include "Game.h"

Game::Game() : m_window("Working game loop!", sf::Vector2u(1280, 720))
{
}


Game::~Game()
{
}

void Game::HandleInput()
{
}

void Game::Update()
{
	m_window.Update();
	float resetTime = 0.01f;
	if (m_elapsed >= resetTime)
	{
		m_sorting.AddStick();
		m_sorting.InsertionSort();
		m_elapsed -= resetTime; //reseting elapsed time to 0 
	}
}

void Game::Render()
{
	m_window.BeginDraw();
	m_sorting.Render(*m_window.GetRenderWindow());
	m_window.EndDraw();
}

Window* Game::GetWindow()
{
	return &m_window;
}

void Game::ResetClock()
{
	m_elapsed += m_clock.restart().asSeconds(); //gets seconds passed
}
