#include "Sorting.h"

Sorting::Sorting() : m_increment(0), height(720), m_stickAmount(256), m_j(0), m_rectWidth(1), m_i(1), m_key(0)
{
	m_line.rotate(90);
	m_maxSize = m_stickAmount * 5;
}

Sorting::~Sorting()
{
}

void Sorting::Render(sf::RenderWindow &l_renderWindow)
{
	for (auto& line : m_lines)
	{
		l_renderWindow.draw(line);
	}
}

void Sorting::AddStick()
{
	if (m_lines.size() >= m_stickAmount) return;
	m_increment += 5;
	std::cout << m_increment << std::endl;
	int randNum = rand() % (450 - 50 + 1) + 50; // 50 - min / 450 - max
	m_line.setSize(sf::Vector2f(-randNum, 1));
	m_line.setPosition(sf::Vector2f(m_increment, height));
	m_lines.push_back(m_line);
}

void Sorting::BubbleSort()
{
	if (m_increment != m_maxSize) return;
	if (m_lines[m_j].getSize().x < m_lines[m_j + 1].getSize().x)
	{
		float temp = m_lines[m_j].getSize().x;

		m_lines[m_j].setSize(sf::Vector2f(m_lines[m_j + 1].getSize().x, m_rectWidth));
		m_lines[m_j + 1].setSize(sf::Vector2f(temp, m_rectWidth));
	}
	++m_j;
	if (m_j == m_lines.size() - 1) m_j = 0;
}


VA_FIX("Revise function maybe optimize it abit")
void Sorting::InsertionSort()
{
	if (m_increment != m_maxSize) return;
	m_key = m_lines[m_i].getSize().x;
	m_j = m_i - 1;
	if (m_j >= 0 && m_lines[m_j].getSize().x > m_key)
	{
		m_lines[m_j + 1].setSize(sf::Vector2f(m_lines[m_j].getSize().x, m_rectWidth));
		m_j = m_j - 1;
	}
	m_lines[m_j + 1].setSize(sf::Vector2f(m_key, m_rectWidth));
	m_i++;
	if (m_i == m_lines.size()) m_i = 1;
	std::cout << m_lines.size() << std::endl;
}